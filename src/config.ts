const PORT = process.env.PORT || 4001;

export const config = {
  server: {
    port: PORT,
    secret: 'secret',
  },
};
