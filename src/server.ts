import express from 'express';
import { createServer } from 'http';
import { Server } from 'socket.io';
import mongoose from 'mongoose';
import * as usersController from './controllers/users';
import * as boardsController from './controllers/boards';
import * as columnsController from './controllers/columns';
import * as tasksController from './controllers/tasks';
import bodyParser from 'body-parser';
import authMiddleware from './middlewares/auth';
import cors from 'cors';
import { SocketEventsEnum } from './types/socketEvents.enum';
import jwt from 'jsonwebtoken';

import User from './models/user';
import { SocketInterface } from './types/socketInterface.interface';
import { config } from './config';

const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: '*',
  },
});

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// converts "_id" to "id"
mongoose.set('toJSON', {
  virtuals: true,
  transform: (_, converted) => {
    delete converted._id;
  },
});

// Start
app.get('/', (req, res) => {
  res.send('API IS UP');
  res.end();
});

//  Auth
app.get('/api/user', authMiddleware, usersController.currentUser);
app.post('/api/users', usersController.signup);
app.post('/api/users/login', usersController.login);

// Boards
app.get('/api/boards', authMiddleware, boardsController.getBoards);
app.get('/api/boards/:boardId', authMiddleware, boardsController.getBoard);
app.post('/api/boards', authMiddleware, boardsController.createBoard);

// Columns
app.get(
  '/api/boards/:boardId/columns',
  authMiddleware,
  columnsController.getColumns
);

// Tasks
app.get('/api/boards/:boardId/tasks', authMiddleware, tasksController.getTasks);

export default app;
// Socket.io
io.use(async (socket: SocketInterface, next) => {
  try {
    const token = (socket.handshake.auth.token as string) ?? '';
    const data = jwt.verify(token.split(' ')[1], config.server.secret) as {
      id: string;
      email: string;
    };

    const user = await User.findById(data.id);

    if (!user) {
      return next(new Error('Authentication error'));
    }

    socket.user = user;
    next();
  } catch (err) {
    next(new Error('Authentication error'));
  }
}).on('connection', (socket) => {
  socket.on(SocketEventsEnum.boardsJoin, (data) => {
    boardsController.joinBoard(io, socket, data);
  });
  socket.on(SocketEventsEnum.boardsLeave, (data) => {
    boardsController.leaveBoard(io, socket, data);
  });
  socket.on(SocketEventsEnum.columnsCreate, (data) => {
    columnsController.createColumn(io, socket, data);
  });
  socket.on(SocketEventsEnum.tasksCreate, (data) => {
    tasksController.createTask(io, socket, data);
  });
  socket.on(SocketEventsEnum.boardsUpdate, (data) => {
    boardsController.updateBoard(io, socket, data);
  });

  socket.on(SocketEventsEnum.boardsDelete, (data) => {
    boardsController.deleteBoard(io, socket, data);
  });

  socket.on(SocketEventsEnum.columnsDelete, (data) => {
    columnsController.deleteColumn(io, socket, data);
  });

  socket.on(SocketEventsEnum.columnsUpdate, (data) => {
    columnsController.updateColumn(io, socket, data);
  });

  socket.on(SocketEventsEnum.tasksUpdate, (data) => {
    tasksController.updateTask(io, socket, data);
  });

  socket.on(SocketEventsEnum.tasksDelete, (data) => {
    tasksController.deleteTask(io, socket, data);
  });
});

mongoose
  .connect(
    'mongodb+srv://katherinemonroy:JmNLXJipyO24neju@cluster0.hpy0xif.mongodb.net/node-angular?retryWrites=true&w=majority'
  )
  .then(() => {
    console.log('connected to mongodb');
    httpServer.listen(4001, () => {
      console.log(`API is listening on port 4001`);
    });
  });
