export enum SocketEventsEnum {
  boardsJoin = 'boards:join',
  boardsLeave = 'boards:leave',
  boardsUpdate = 'boards:updated',

  boardsUpdateSuccess = 'boards:updateSuccess',
  boardsUpdateFailure = 'boards:updateFailure',

  boardsDelete = 'boards:deleted',
  boardsDeleteSuccess = 'boards:deleteleteSuccess',
  boardsDeleteFailure = 'boards:deleteFailure',

  columnsCreate = 'columns:create',
  columnsCreateSuccess = 'columns:createSuccess',
  columnsCreateFailure = 'columns:createFailure',

  columnsDelete = 'columns:deleted',
  columnsDeleteSuccess = 'columns:deletedSuccess',
  columnsDeleteFailure = 'columns:deletedFailure',

  columnsUpdate = 'columns:update',
  columnsUpdateSuccess = 'columns:updateSuccess',
  columnsUpdateFailure = 'columns:updateFailure',

  tasksCreate = 'tasks:create',
  tasksCreateSuccess = 'tasks:createSuccess',
  tasksCreateFailure = 'tasks:createFailure',

  tasksUpdate = 'tasks:update',
  tasksUpdateSuccess = 'tasks:updateSuccess',
  tasksUpdateFailure = 'tasks:updateFailure',

  tasksDelete = 'tasks:delete',
  tasksDeleteSuccess = 'tasks:deleteSuccess',
  tasksDeleteFailure = 'tasks:deleteFailure',
}
